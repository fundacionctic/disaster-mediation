// uncomment and execute sbt gen-idea to generate idea metafiles
addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.1.0")

// for graph deps
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.7.4")

// to bundle all mediation-core + libraries on single jar 
// addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.10.0")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.9.1")

// addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "2.1.1")