name := "mediation-core"

version := "0.1-SNAPSHOT"

organization := "es.ctic.disaster"

scalaVersion := "2.10.3"

// logging: grizzled + logback
libraryDependencies += "org.clapper" %% "grizzled-slf4j" % "1.0.1"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.0.7"

// jena
libraryDependencies += "com.hp.hpl.jena" % "jena" % "2.6.3" exclude("org.slf4j", "slf4j-log4j12")

// jena arq
libraryDependencies += "com.hp.hpl.jena" % "arq" % "2.8.8"  exclude("org.slf4j", "slf4j-log4j12")

// apache commons-cli parser
libraryDependencies += "commons-cli" % "commons-cli" % "1.2"

// Test: junit
libraryDependencies += "junit" % "junit" % "4.8.1" % "test"

// Test: ScalaTest
libraryDependencies += "org.scalatest" %% "scalatest" % "1.9.1" % "test"

// Test: ScalaMock
// libraryDependencies += "org.scalamock" %% "scalamock-scalatest-support" % "2.4" % "test"

// For ScalaMock
// resolvers += "Sonatype OSS Releases" at "http://oss.sonatype.org/content/repositories/releases/"

resolvers += "3rd party repo" at "http://devit.fundacionctic.org:8081/nexus/content/repositories/thirdparty/"

resolvers += "CTIC public releases" at "http://devit.fundacionctic.org:8081/nexus/content/repositories/releases/"

resolvers += "CTIC private releases" at "http://devit.fundacionctic.org:8081/nexus/content/repositories/private-releases/"

resolvers += "CTIC private snapshots"  at "http://devit.fundacionctic.org:8081/nexus/content/repositories/private-snapshots/"

// solve sbt assembly
resolvers += "sbt-assembly-resolver-0" at "http://scalasbt.artifactoryonline.com/scalasbt/sbt-plugin-releases"

// local mvn repo
// FIXME configure maven repo at ~/.sbt/local.sbt
// resolvers += "Local Maven Repository" at "file:///home/gillagher/.m2/repository"

// Invoke with sbt publish-local
// publishTo := Some(Resolver.file("My local maven repo", file(Path.userHome + "/.m2/repository")))
publishTo := Some(Resolver.file("file",  new File(Path.userHome.absolutePath+"/.m2/repository")))

// Invoke with sbt publish (requires extra configuration)
publishTo <<= (version) { version: String =>
	val nexus = "http://devit.fundacionctic.org:8081/nexus/content/repositories/"
   		if (version.trim.endsWith("SNAPSHOT")) Some("snapshots" at nexus + "private-snapshots/")
  		else                                   Some("releases"  at nexus + "private-releases/")
}

credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")

// for sbt-dependency-graph plugin
// run with sbt dependency-graph
net.virtualvoid.sbt.graph.Plugin.graphSettings