/*
This file is part of the FP7 project DISASTER <www.disaster-fp7.eu>

The research leading to these results has received funding from the 
European Union Seventh Framework Programme (FP7/2007-2013) 
under grant agreement nº 285069

 Copyright [2012-2013] [Fundación CTIC <www.fundacionctic.org>]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package es.ctic.disaster.mediation

import java.io._
import grizzled.slf4j.Logging
import org.apache.commons.cli.{ Options, PosixParser, CommandLine, HelpFormatter }

object CLI extends Logging {
  lazy val mediator = new MediationComponentImpl()
  val OPTION_EMERGEL: String = "e"
  val OPTION_OUTPUT: String = "o"

  def main(args: Array[String]) {
    val options = new Options()
    options.addOption(OPTION_OUTPUT, true, "path to the output RDF file")
    options.addOption(OPTION_EMERGEL, true, "path to emergel RDF file")
    val cliParser = new PosixParser()
    try {
      // parse input
      val cmd: CommandLine = cliParser.parse(options, args)
      if (cmd.getArgs.size < 2) throw new org.apache.commons.cli.ParseException("no input files")
      val inputFiles: Seq[File] = cmd.getArgs.map(new File(_))
      // read jenarules
      mediator.addAssemblyRules(inputFiles(0))
      // read emergel
      if (cmd hasOption OPTION_EMERGEL) mediator.addMetadata(new File(cmd getOptionValue OPTION_EMERGEL))
      // read input rdf
      mediator.addMetadata(inputFiles(1))
      // process output	
      val modelResult = mediator.execute()
      logger.debug("Writing output (" + modelResult.size + " triples)")
      val os: OutputStream = if (cmd hasOption OPTION_OUTPUT) new FileOutputStream(cmd getOptionValue OPTION_OUTPUT) else System.out
      modelResult.write(os, "RDF/XML")
      if (cmd hasOption OPTION_OUTPUT) { os.close() }
    } catch {
      case e: org.apache.commons.cli.ParseException =>
        System.err.println(e.getMessage)
        new HelpFormatter().printHelp("mediation [OPTIONS] [TRANSFORMATION RULES] [RDF INPUT]", options)
      case e: Exception => logger.error("Internal error", e)
    }
  }
}