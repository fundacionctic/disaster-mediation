/*
This file is part of the FP7 project DISASTER <www.disaster-fp7.eu>

The research leading to these results has received funding from the 
European Union Seventh Framework Programme (FP7/2007-2013) 
under grant agreement nº 285069

 Copyright [2012-2013] [Fundación CTIC <www.fundacionctic.org>]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package es.ctic.disaster.mediation

import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.net.URL
import java.util.List

import scala.collection.JavaConversions._

import grizzled.slf4j.Logging
import com.hp.hpl.jena.rdf.model.Model
import com.hp.hpl.jena.rdf.model.ModelFactory
import com.hp.hpl.jena.reasoner.rulesys.GenericRuleReasoner
import com.hp.hpl.jena.reasoner.rulesys.Rule

class MediationComponentImpl extends MediationComponent with Logging {

  var rules: Seq[Rule] = Seq()
  var model = ModelFactory.createDefaultModel()
  val emergelFilename = "emergel-all.rdf"
  lazy val emergelPreloaded = ModelFactory.createDefaultModel()
    .read(this.getClass.getResourceAsStream(emergelFilename), null)

  def addRules(rulesUrl: URL) {
    info("Loading Jena rules from URL: " + rulesUrl)
    rules = rules ++ Rule.rulesFromURL(rulesUrl.toString())
  }

  def addAssemblyRules(rulesFile: File) {
    info("Loading Jena rules from file: " + rulesFile)
    rules = rules ++ Rule.rulesFromURL("file://" + rulesFile.getAbsolutePath())
  }

  def addAssemblyRules(inputRules: Seq[Rule]) {
    info("Loading Jena rules from list of rules: " + inputRules);
    rules = rules ++ inputRules
  }

  def addMetadata(metadataFile: File) {
    info("Loading RDF from file: " + metadataFile)
    model.read(new FileInputStream(metadataFile), null)
  }

  def addMetadata(metadataUrl: URL) {
    info("Loading RDF from URL: " + metadataUrl)
    model.read(metadataUrl.toString, null)
  }

  def addMetadata(metadataUrl: String) {
    info("Loading RDF from URL: " + metadataUrl)
    model.read(metadataUrl, null)
  }

  def addMetadata(metadataStream: InputStream) {
    info("Loading RDF from stream: " + metadataStream)
    model.read(metadataStream, null)
  }

  def addMetadata(metadataModel: Model) {
    info("Loading RDF from model: " + metadataModel)
    model.add(metadataModel)
  }

  def execute(): Model = {
    val reasoner = new GenericRuleReasoner(rules)
    ModelFactory.createInfModel(reasoner, model)
  }

  override def transform(inputModel: Model, emergelModel: Model, transformationRules: List[Rule]): Model = {
    addMetadata(inputModel)
    addMetadata(emergelModel)
    addAssemblyRules(transformationRules)
    execute()
  }

  override def transform(inputModel: Model, transformationRules: List[Rule]): Model = {
    if (emergelPreloaded.isEmpty)
      warn("emergel preloaded is empty")
    transform(inputModel, emergelPreloaded, transformationRules)
  }

  override def transform(inputModel: Model, emergelUrl: URL, transformationRules: List[Rule]): Model = {
    addMetadata(inputModel)
    addMetadata(emergelUrl)
    addAssemblyRules(transformationRules)
    execute()
  }
}