/*
This file is part of the FP7 project DISASTER <www.disaster-fp7.eu>

The research leading to these results has received funding from the 
European Union Seventh Framework Programme (FP7/2007-2013) 
under grant agreement nº 285069

 Copyright [2012-2013] [Fundación CTIC <www.fundacionctic.org>]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package es.ctic.disaster.mediation

import grizzled.slf4j.Logging
import collection.JavaConversions._
import java.lang.Boolean
import java.io.ByteArrayOutputStream
import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import scala.xml.Elem

/**
 * Created with IntelliJ IDEA.
 * User: Guillermo Gonzalez-Moriyon
 * Date: 5/20/13
 * Time: 10:43 AM 
 */
class MediationComponentImplTest()
  extends MediationComponentImpl with FunSuite with Logging with ShouldMatchers {

    def fixture1 = new {
      val rulesFilename = "dk-to-de.jenarules"      
      val rulesFile = this.getClass.getClassLoader.getResource(rulesFilename)
      addRules(rulesFile)
      val emergelModelFilename = "all-classifications.rdf"
      val emergelModelFile = this.getClass.getClassLoader.getResource(emergelModelFilename)
      addMetadata(emergelModelFile)
      val modelFilename = "ekofisk-dk.rdf"
      val modelFile = this.getClass.getClassLoader.getResource(modelFilename)
      addMetadata(modelFile)
      val model = execute()
      val os = new ByteArrayOutputStream()
      model.write(os, "RDF/XML")
      val modelAsString = new String(os.toByteArray(), "UTF-8")
    }

    def fixture2 = new {
      val rulesFilename = "dk-to-de.jenarules"      
      val rulesFile = this.getClass.getClassLoader.getResource(rulesFilename)
      addRules(rulesFile)
      val emergelModelFilename = "all-classifications-one.rdf"
      val emergelModelFile = this.getClass.getClassLoader.getResource(emergelModelFilename)
      addMetadata(emergelModelFile)
      val modelFilename = "ekofisk-dk-oneresource.rdf"
      val modelFile = this.getClass.getClassLoader.getResource(modelFilename)
      addMetadata(modelFile)
      val model = execute()
      val os = new ByteArrayOutputStream()
      model.write(os, "RDF/XML")
      val modelAsString = new String(os.toByteArray(), "UTF-8")
    }

    ignore("test if rules are applied on complete model") {
      fixture1.modelAsString should include ("<rdf:Description rdf:about=\"http://localhost:8080/tabels/project/ekofisk-dk/resource/1\">\n    <dct:subject rdf:resource=\"http://localhost:8080/tabels/project/german-classification/resource/10.2.6.10\"/>")
    }

    test("test if rules are applied on lite model") {
      // println("output: " + fixture2.modelAsString)
      fixture2.modelAsString should include ("<rdf:Description rdf:about=\"http://localhost:8080/tabels/project/ekofisk-dk/resource/1\">\n    <dct:subject rdf:resource=\"http://localhost:8080/tabels/project/german-classification/resource/10.2.6.10\"/>")
    }
}
