# mediation-core

Mediation component library from [DISASTER project](https://www.disaster-fp7.eu) by [Fundación CTIC](https://www.fundacionctic.org)

----
## License

All projects are licensed under the Apache 2.0 license. Read each project LICENSE file for further information.

----
## Requirements
* [Scala 2.10.3](http://scala-lang.org/)
* [SBT 0.12.2](http://www.scala-sbt.org/0.12.2/docs/)


----
## Getting started

Example of execution on the command line:
      
      &> sbt "run-main es.ctic.disaster.mediation.CLI -o output.rdf \
      -e src/test/resources/all-classifications-one.rdf  \
      src/test/resources/dk-to-de.jenarules \
      src/test/resources/ekofisk-dk-oneresource.rdf"

----
## Single executable jar      

You can obtain a single jar containing the mediation component and all its dependencies 
using the sbt assembly plugin. Execute the following to create the bundle:

      &> sbt assembly

To execute:

      &> java -jar target/target/scala-2.10/mediation-core-assembly-0.1-SNAPSHOT.jar \
      -o output.rdf \
      -e src/test/resources/all-classifications-one.rdf  \
      src/test/resources/dk-to-de.jenarules \
      src/test/resources/ekofisk-dk-oneresource.rdf
	  
----
## Team
Mediation component has been developed within [Fundación CTIC](https://www.fundacionctic.org) by:

* Carlos Tejo-Alonso 
* Guillermo Gonzalez-Moriyon 	<mailto:guillermo.gonzalez@fundacionctic.org>
			

      
----
## Contact
		
* Guillermo Gonzalez-Moriyon 	<mailto:guillermo.gonzalez@fundacionctic.org>

----
## Acknowledgements

All rights reserved © [Disaster project](https://www.disaster-fp7.eu)

The research leading to these results has received funding from the 
European Union Seventh Framework Programme (FP7/2007-2013) under grant agreement nº 285069