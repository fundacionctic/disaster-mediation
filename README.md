# DISASTER Mediation Component #

Mediation component from [DISASTER project](https://www.disaster-fp7.eu) by [Fundación CTIC](https://www.fundacionctic.org)

----
## License

All projects are licensed under the Apache 2.0 license. Read each project LICENSE file for further information.