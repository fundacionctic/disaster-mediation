# mediation-website

Mediation componente website from [DISASTER project](https://www.disaster-fp7.eu) by [Fundación CTIC](https://www.fundacionctic.org)

----
## License
All projects are licensed under the Apache 2.0 license. Read each project LICENSE file for further information.

----
## Requirements
* JDK 6 
* [play 2.1](http://www.playframework.org)

----
## Getting started
1. Go to project root directory
2. Start play console ($>play)
3. Launch the application server (play>run)
4. Run [http://localhost:9000/disaster-fp7-mediation/](http://localhost:9000/disaster-fp7-mediation/) in your web browser and have fun!

	  
----
## Team
Mediation component website has been developed within [Fundación CTIC](https://www.fundacionctic.org) by:

* Guillermo Gonzalez-Moriyon 	<mailto:guillermo.gonzalez@fundacionctic.org>
* Carlos Tejo-Alonso 			<mailto:carlos.tejo@fundacionctic.org>
* Alfonso Noriega				<mailto:alfonso.noriega@fundacionctic.org>
* Emilio Rubiera				<mailto:emilio.rubiera@fundacionctic.org>
* Marc Segond					<mailto:marc.segond@fundacionctic.org>

      
----
## Contact

* Carlos Tejo-Alonso 			<mailto:carlos.tejo@fundacionctic.org>
* Guillermo Gonzalez-Moriyon 	<mailto:guillermo.gonzalez@fundacionctic.org>

----
## Acknowledgements

All rights reserved © [Disaster project](https://www.disaster-fp7.eu)

The research leading to these results has received funding from the 
European Union Seventh Framework Programme (FP7/2007-2013) under grant agreement nº 285069