import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "mediation-website"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    "org.webjars" %% "webjars-play" % "2.1.0-3" exclude("play", "*"),
    "org.webjars" % "bootstrap" % "2.3.2",
	   "es.ctic.disaster" %% "mediation-core" % "0.1-SNAPSHOT"
  )


  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here
    resolvers := Seq(
         // "sonatype-snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
         "restlet repo" at "http://maven.restlet.com/",
         Resolver.file("Local repo", file(System.getProperty("user.home") + "/.ivy2/local"))(Resolver.ivyStylePatterns),
         "3rd party repo"  at "http://devit.fundacionctic.org:8081/nexus/content/repositories/thirdparty/",
         "CTIC releases"   at "http://devit.fundacionctic.org:8081/nexus/content/repositories/private-releases/",
         "CTIC snapshots"  at "http://devit.fundacionctic.org:8081/nexus/content/repositories/private-snapshots/"
    ),
    credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")	
  )

}
