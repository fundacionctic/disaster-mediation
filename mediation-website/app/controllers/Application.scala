package controllers

import play.api._
import play.api.mvc._
import play.api.Play.current

object Application extends Controller {
  
  def index = Action {implicit request =>
    Ok(views.html.index())
  }
  
  def mediation = Action(parse.multipartFormData) { implicit request =>
    import java.io.File
  	import es.ctic.disaster.mediation._

  	val input_rdf:File = request.body.file("input_rdf").get.ref.file
	val input_jenarules:File = request.body.file("input_jenarules").get.ref.file
	val input_model:File = request.body.file("input_model").get.ref.file
	
	val result = (input_rdf, input_jenarules, input_model) match {
	
		case (g, _ , _) if (g.length==0) => {
        	Redirect(routes.Application.index).flashing(
				"error" -> "Missing input graph file"
			)
		}
	
		case (_, g, _) if(g.length==0) => {
        	Redirect(routes.Application.index).flashing(
          	  "error" -> "Missing transformation rules file"
        	)
		}

		case (_, _, g) if(g.length==0) => {
        	Redirect(routes.Application.index).flashing(
          	  "error" -> "Missing model file"
        	)
		}
	
		case default =>  {
	
			val mc:MediationComponentImpl = new MediationComponentImpl
	
			// populate model and rules
			mc.addMetadata(input_rdf)
			mc.addAssemblyRules(input_jenarules)
	    	mc.addMetadata(input_model)

			// execute mediation
			val model = mc.execute()
	
			// Generate Output Filename
			import java.util.Calendar
			import java.text.SimpleDateFormat
			val format1:SimpleDateFormat  = new SimpleDateFormat("yyyyMMddHHmmss");
			val now = format1.format(Calendar.getInstance().getTime());
			val outputFilename:String = Play.current.configuration.getString("output.filename").get
			val generatedOutputFilename = now + "_" + outputFilename
  
			// Send the output model
			import play.api.libs.iteratee.Enumerator
			val dataContent: Enumerator[Array[Byte]] = Enumerator.outputStream( os => {
				model.write(os,"RDF/XML")
				os.close
			})

			Ok.stream(dataContent >>> Enumerator.eof).withHeaders(
				"Content-Type"->"application/rdf+xml", 
				"Content-Disposition"->"attachment; filename=".concat(generatedOutputFilename)
			)
		}
	}
	// return the result
	result
}
}	

	/*
    request.body.file("input_rdf").map { input_rdf =>

  	  val mc:MediationComponentImpl = new MediationComponentImpl

	  val filename = input_rdf.filename 
      val contentType = input_rdf.contentType
	  println("filename="+filename)
      //input_rdf.ref.moveTo(new File("/tmp/input_rdf"))
      val tmpFile:File = input_rdf.ref.file
	  println("tmpFile.length="+tmpFile.length)
	  

      //request.body.file("input_jenarules"
	  
      val rulesFilename = "dk-to-de.jenarules"      
      val rulesFile = play.api.Play.getFile("app/resources/"+rulesFilename) 	  
	  println("========> "+rulesFile.length)
      mc.addAssemblyRules(rulesFile)
	  
      val modelFilename = "ekofisk-dk-oneresource.rdf"
      val modelFile = play.api.Play.getFile("app/resources/"+modelFilename)
	  println("========> "+modelFile.length)

      mc.addMetadata(tmpFile)

      val emergelModelFilename = "all-classifications-one.rdf"
      val emergelModelFile = play.api.Play.getFile("app/resources/"+emergelModelFilename) 	
	  println("========> "+emergelModelFile.length)
      mc.addMetadata(emergelModelFile)
	  
      
	  val model = mc.execute()
*/
	  // File approach
/*
	  import java.io.FileOutputStream
	  import java.io.FileWriter

	  val fos:FileOutputStream = new FileOutputStream("/Users/dayures/tmp/foo.rdf") // create a file output stream around f
	  model.write(fos,"RDF/XML")

	Ok.sendFile(
    	content = new java.io.File("/Users/dayures/tmp/foo.rdf"),
		fileName = _ => generatedOutputFilename
   )

//	  def out = new FileWriter( "/Users/dayures/tmp/writer.rdf" );
//	  model.write(out,"RDF/XML")	  

*/
	/*
	  // Generate Output Filename
	  import java.util.Calendar
	  import java.text.SimpleDateFormat
	  val format1:SimpleDateFormat  = new SimpleDateFormat("yyyyMMddHHmmss");
	  val now = format1.format(Calendar.getInstance().getTime());
	  val outputFilename:String = Play.current.configuration.getString("output.filename").get
	  val generatedOutputFilename = now + "_" + outputFilename
	  
	  // Send the output model
	  import play.api.libs.iteratee.Enumerator
	  val dataContent: Enumerator[Array[Byte]] = Enumerator.outputStream( os => {
		  model.write(os,"RDF/XML")
		  os.close
	  }
	  )

      Ok.stream(dataContent >>> Enumerator.eof).withHeaders(
        "Content-Type"->"application/rdf+xml", 
        "Content-Disposition"->"attachment; filename=".concat(generatedOutputFilename)
      )
	  
	  
    }.getOrElse {
  	  println("Error!")
      Redirect(routes.Application.index).flashing(
        "error" -> "Missing file"
      )
    }
  }
  */
  
//Get all files bound to the form when submitted 
//List<FilePart> plate_files = request().body().asMultipartFormData().getFiles();
//Get files from a specific name or id
//FilePart myfile = request().body().asMultipartFormData().getFile("files");