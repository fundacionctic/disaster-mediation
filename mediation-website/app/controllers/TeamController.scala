/*
This file is part of the FP7 project DISASTER <www.disaster-fp7.eu>

The research leading to these results has received funding from the European Union Seventh Framework Programme (FP7/2007-2013) under grant agreement nº 285069

 Copyright [2012-2013] [Fundación CTIC <www.fundacionctic.org>]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

/*
	@author Carlos Tejo-Alonso <www.dayures.net>
	@author Guillermo Gonzalez-Moriyon <guillermo.gonzalez@fundacionctic.org>
*/

package controllers

import play.api._
import play.api.mvc._
import play.api.Play.current

object TeamController extends Controller {
  
  /**
   * team method
   */
  def team = Action {
    Ok(views.html.team())
  } 
}